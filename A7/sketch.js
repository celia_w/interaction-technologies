// Declare global variables.
// If these were inside a function you could only call upon them in that specific function not outside of it (local variables).

let mode = 0 //Start the mode as 0, aka screen 1
let bgcolor = "#313132";
let bgcolor2 = "#E0E0E0";

function setup() {
	createCanvas(375, 700);
}

function draw() {
  background(0);

	//Create the button, which will change from screen 1 to screen 2. on top of 1 picture
	pictureTrigger = createButton('View post');
	pictureTrigger.position(17, 111);
	pictureTrigger.size(110, 110);
	pictureTrigger.style("background", "#FFF");
	pictureTrigger.style("color", "#313132");
	pictureTrigger.style("border", "none");
	pictureTrigger.style("border-radius", "10px");
	pictureTrigger.style("font-size", "1.1em");
	pictureTrigger.style("font-weight", "normal");
	pictureTrigger.style("text-align", "center");
	pictureTrigger.style("font-family", "Arial");
	pictureTrigger.mousePressed(viewPost);

	//Here we change from screen 1 to screen 2, based on the "submit" button action (openBlackBox).
  if (mode == 0) {
    screen1();
  } else if (mode == 1) {
    screen2();
  }
}

//Useful to have multiple elements on one screen
function screen1() {

	//title of tracker gallery
	push();
	fill(255);
	textSize(34);
	textFont("Arial");
	text("Title", 16, 40); //text + position
	pop();

	//search bar
	push();
	fill(bgcolor);
	noStroke();
	rect(16, 50, 344, 43, 50);

	//text in search bar
	fill(bgcolor2);
	textSize(17);
	textAlign(CENTER, CENTER);
	textFont("Arial");
	text("Search", 55, 70); //text + position
	pop();

	//pics
	push();
	fill(255);
	rect(16, 110, 110, 110, 10); //x, y, height, length, roundness
	rect(135, 110, 110, 110, 10); //x, y, height, length, roundness
	rect(254, 110, 110, 110, 10); //x, y, height, length, roundness

	rect(16, 250, 110, 110, 10); //x, y, height, length, roundness
	rect(135, 250, 110, 110, 10); //x, y, height, length, roundness
	rect(254, 250, 110, 110, 10); //x, y, height, length, roundness

	rect(16, 390, 110, 110, 10); //x, y, height, length, roundness
	rect(135, 390, 110, 110, 10); //x, y, height, length, roundness
	rect(254, 390, 110, 110, 10); //x, y, height, length, roundness
	pop();

	//positions of title/date text
	push();
	fill(255);
	textSize(12);
	textFont("Arial");
	text("title/date", 16, 235); //text + position
	text("title/date", 135, 235); //text + position
	text("title/date", 254, 235); //text + position

	text("title/date", 16, 375); //text + position
	text("title/date", 135, 375); //text + position
	text("title/date", 254, 375); //text + position

	text("title/date", 16, 515); //text + position
	text("title/date", 135, 515); //text + position
	text("title/date", 254, 515); //text + position
	pop();
}

function screen2() {
	background(0);

	//button same colour as picture
	pictureTrigger.style("color", "#FFF");

	//picture
	rect(16, 110, 344, 344, 10)

	//back
	push();
	fill(255);
	textSize(20);
	textFont("Arial");
	text("'Back button here'", 16, 65); //text + position
	pop();

	//titles
	push();
	fill(255);
	textSize(20);
	textFont("Arial");
	text("Time", 16, 480);
	text("Medium", 170, 480);
	text("References", 16, 550);
	pop();

	//text under titles
	push();
	fill(255);
	textSize(17);
	textFont("Arial");
	text("More than 1 hour", 16, 500);
	text("Digital", 170, 500);
	text("Insert link here", 16, 570);
	text("Insert extra notes here", 16, 620);
	pop();
}

/*This function reacts to clicking on a rectangle.
It changes the mode from 0 to 1, which then changes the "screen" (see draw function)*/
function viewPost() {
	mode = 1;
}
